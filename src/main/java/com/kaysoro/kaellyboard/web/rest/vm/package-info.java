/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kaysoro.kaellyboard.web.rest.vm;
